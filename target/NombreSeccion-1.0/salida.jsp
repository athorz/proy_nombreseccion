<%-- 
    Document   : salida
    Created on : 30-03-2020, 19:15:29
    Author     : jarqu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hey!</h1>
        <%
            String nombre = (String) request.getAttribute("nombre");
            int seccion = (int) request.getAttribute("seccion");
        %>
        <p>Amigo(a) <%= nombre %></p>
        <p>Tu sección es: <%=seccion%> </p>
    </body>
</html>
